import json
import ssl
from datetime import date, timedelta
import tweepy
import requests
import urllib
import io
print("""
🥚🥚🥚 🐥 Fabulous Wikipedia Twitter Bot 🐥 🥚🥚🥚
""")

# get secret keys stuff 
keyFile = open('keys', 'r')
lines = keyFile.readlines()
CONSUMER_KEY        = lines[0].rstrip()
CONSUMER_SECRET     = lines[1].rstrip()
ACCESS_TOKEN        = lines[2].rstrip()
ACCESS_TOKEN_SECRET = lines[3].rstrip()

# authorize tweepy
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)

# user_agent for requests
user_agent = {'User-agent': 'Mozilla/5.0'}

# if using this code for another language, change language below and adapt blacklist accordingly

language = "nl"

blacklist = ["Hoofdpagina", "Speciaal:Zoeken", "Special:MyPage/zeusmodepreferences.js", "Speciaal:RecenteWijzigingen"]

# make date variables
yesterday = date.today() - timedelta(days=1)
year = yesterday.strftime("%Y")
month = yesterday.strftime("%m")
day = yesterday.strftime("%d")
printday = yesterday.strftime("%-e")
months_in_dutch = ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"]
printmonth = months_in_dutch[int(month)-1]

# function to vary the tweet text according to the amount of views
def how_many_times(amount):
    if amount > 150000:
        return "een verontrustende"
    elif amount > 100000:
        return "een astronomische"
    elif amount > 50000:
        return "een gigantische"
    elif amount > 30000:
        return "een indrukwekkende"
    elif amount > 21000:
        return "maar liefst"
    elif amount > 15000:
        return "ongeveer exact"
    elif amount > 11000:
        return "minstens"
    elif amount > 9000:
        return "op de kop af"
    else:
        return "zowaar"

# function to turn the amount of views into a printable number with thousands separators
def number_with_thousands_separators(nr):
    nr = str(nr)
    thousands_separator = "."
    print_amount = ""
    counter = 0
    for i in range(len(nr)-1,-1,-1):
        counter += 1
        print_amount = nr[i] + print_amount
        if counter % 3 == 0:
            print_amount = thousands_separator + print_amount
    if print_amount[0] == thousands_separator:
        print_amount = print_amount[1:]
    return print_amount

def tweet_tweet():
    # get yesterday's top viewed pages
    req_url = "https://wikimedia.org/api/rest_v1/metrics/pageviews/top/{}.wikipedia.org/all-access/{}/{}/{}".format(language, year, month, day)
    r = requests.get(req_url, headers = user_agent)
    response_data = r.json()
    # determine top viewed page, excluding blacklisted items
    for i in range(5):
        most_viewed_page = response_data["items"][0]["articles"][i]['article']
        if most_viewed_page in blacklist:
            continue
        else:
            most_viewed = response_data["items"][0]["articles"][i]['article']
            amount_of_views = response_data["items"][0]["articles"][i]['views']
            break
    most_viewed_url = "https://" + language + ".wikipedia.org/wiki/" + most_viewed
    print("🥇 " + most_viewed_url)
    # get page info
    page_info_url = "https://{}.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles={}".format(language, most_viewed)
    r = requests.get(page_info_url, headers = user_agent)
    response = r.json()
    try:
        top_title = response["query"]["normalized"][0]["to"]
        print(top_title)
    except:
        print("no top title yet...")
    # try to find picture and title from page info 
    try:
        number_dict = response["query"]["pages"]
        number = list(number_dict.keys())[0]
        top_title = response["query"]["pages"][number]["title"]
        print(top_title)
        picture_url = response["query"]["pages"][number]["original"]["source"]
        print(picture_url)
    except:
        picture_url = None
        print("🚫 no picture here")
    # make print message
    print_message = """\"{}\" was de meest bekeken wikipediapagina van {} {} {}, {} {} keer!
{}""".format(top_title, printday, printmonth, year, how_many_times(amount_of_views), number_with_thousands_separators(amount_of_views), most_viewed_url)
    print("📝  print message is: " + print_message)
    # image stuff
    if picture_url is not None:
        try:
            data = urllib.request.urlopen(picture_url).read()
            file_like_object = io.BytesIO(data) 
            api.update_status_with_media(print_message, 'fake_name.jpg', file=file_like_object)
            print("🐦 Yay: Tweeted a tweet with a picture")
        except Exception as e:
            print(e)
            api.update_status(print_message)
            (print("🐦 Yay: Tweeted a tweet. Picture unfit for tweet."))
    else:
        api.update_status(print_message)
        (print("🐦 Yay: Tweeted a tweet, no picture"))

tweet_tweet()