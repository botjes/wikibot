## Hi!
This is a script that finds yesterday's most viewed Dutch language Wikipedia page and tweets about it on [@TopWikiNL](https://twitter.com/topwikinl). 

Feel free to make your own version in another language using my code. If you do, please credit me and let me know, so I can follow your bot!


